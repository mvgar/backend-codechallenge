<?php
/*María Velasco Garcia*/
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Clientes;
use AppBundle\Entity\Drivers;
use AppBundle\Entity\Pedidos;
use AppBundle\Entity\Direcciones;
use Symfony\Component\HttpKernel\Exception\PreconditionFailedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class PedidosController extends FOSRestController{

	 /**
	 * @Rest\Post("/pedidos/create")
	 */
	 public function createAction(Request $request)
	 {
		$nombre = $request->get('nombre');
		$apellidos = $request->get('apellidos');
		$email = $request->get('email');
		$telefono = $request->get('telefono');
		$fecha_entrega = $request->get('fecha_entrega');
		$hora_inicio = $request->get('hora_inicio');
		$hora_fin = $request->get('hora_fin');
		$calle = $request->get('calle');
		$cod_postal = $request->get('cod_postal');
		$ciudad = $request->get('ciudad');
		$provincia = $request->get('provincia');
		$pais = $request->get('pais');
		
		try{			
			if(empty($nombre))
				throw new PreconditionFailedHttpException("Nombre obligatorio", null, 1001);
			if(empty($apellidos))
				throw new PreconditionFailedHttpException("Apellidos obligatorios", null, 1002);
			if(empty($email))
				throw new PreconditionFailedHttpException("Email obligatorio", null, 1003);
			if(!filter_var($email, FILTER_VALIDATE_EMAIL))
				throw new PreconditionFailedHttpException("Email inválido", null, 1004);
			if(empty($telefono))
				throw new PreconditionFailedHttpException("Teléfono obligatorio", null, 1005);
			if(empty($fecha_entrega))
				throw new PreconditionFailedHttpException("Fecha de entrega obligatoria", null, 1006);
			if(!$this->validar_fecha_espanol($fecha_entrega))
				throw new PreconditionFailedHttpException("Fecha de entrega inválida (yyyy-mm-dd)", null, 1007);
			if(strtotime($fecha_entrega) <= strtotime(date("Y-m-d",time())))
				throw new PreconditionFailedHttpException("Fecha de entrega debe ser superior a la fecha actual", null, 1018);
			if(empty($hora_inicio))
				throw new PreconditionFailedHttpException("Inicio de franja horaria obligatorio", null, 1008);
			if(!$this->validar_hora($hora_inicio))
				throw new PreconditionFailedHttpException("Inicio de franja horaria inválido (hh:mm)", null, 1009);
			if(empty($hora_fin))
				throw new PreconditionFailedHttpException("Fin de franja horaria obligatorio", null, 1010);
			if(!$this->validar_hora($hora_fin))
				throw new PreconditionFailedHttpException("Fin de franja horaria inválido (hh:mm)", null, 1011);
		
			if(!$this->validar_franja_horaria($hora_inicio, $hora_fin))
				throw new PreconditionFailedHttpException("Franja horaria inválida (De 1 a 8h)", null, 1016);
			
			if(empty($calle))
				throw new PreconditionFailedHttpException("Calle obligatoria", null, 1012);
			if(empty($cod_postal))
				throw new PreconditionFailedHttpException("Código postal obligatorio", null, 1013);
			if(empty($ciudad))
				throw new PreconditionFailedHttpException("Ciudad obligatoria", null, 1014);
			if(empty($pais))
				throw new PreconditionFailedHttpException("País obligatorio", null, 1015);
			
			$cliente = $this->getDoctrine()->getRepository(Clientes::class)->findByEmail($email);
						
			 if (!$cliente) {
					 //Insertamos en tabla Clientes -> nuevo cliente
					 $cliente_new = array(
						"email" => $email,
						"nombre" => $nombre,
						"apellidos" => $apellidos,
						"telefono" => $telefono
					 );
					 
					$cliente = $this->getDoctrine()->getRepository(Clientes::class)->insert($cliente_new);					
					
					if(!$cliente->getId())
						throw new HttpException(500, "Error inserción cliente");
				
			} else {
				$id_cliente = $cliente->getId();
			}
			
			
			//Insertamos en tabla Direcciones
			$direccion_new = array(
				"calle" => $calle,
				"codPostal" => $cod_postal,
				"ciudad" => $ciudad,
				"provincia" => $provincia,
				"pais" => $pais
			 );
			 
			$direccion = $this->getDoctrine()->getRepository(Direcciones::class)->insert($direccion_new);	
			
			if(!$direccion->getId())
				throw new HttpException(500, "Error inserción dirección");
				
			$driver = $this->getDoctrine()->getRepository(Drivers::class)->getDriverRandom();	
			
			//Insertamos en tabla Pedidos
			$pedido_new = array(
				"cliente" => $cliente,
				"direccion" => $direccion,
				"fechaEntrega" => $fecha_entrega,
				"horaInicio" => $hora_inicio,
				"horaFin" => $hora_fin,
				"nombreFacturacion" => $nombre,
				"apellidosFacturacion" => $apellidos,
				"telefono" => $telefono,
				"driver" => count($driver)!= 0 ? $driver[0]: null
			 );
			 
			$pedido = $this->getDoctrine()->getRepository(Pedidos::class)->insert($pedido_new);	
					
			if(!$pedido->getId())
				throw new HttpException(500, "Error inserción pedido");
				 
			$id_pedido = $pedido->getId();
			
			$response = [];
			$response["code"] = 0;
			$response["message"] = "";
			$response["id_pedido"] = $id_pedido;
			return $response;
		 }
		 catch( PreconditionFailedHttpException $e ){
			$response = [];
			$response["code"] = $e->getCode();
			$response["message"] = $e->getMessage();
			return $response;
		 }catch( HttpException $e ){
			$response = [];
			$response["code"] = $e->getStatusCode();
			$response["message"] = $e->getMessage();
			return $response;
		 }
		 
	 }
	 

	 /**
	 * @Rest\Get("/pedidos/list")
	 */
	 public function listAction(Request $request)
	 {
		$id = $request->get('id');
		$fecha_entrega = $request->get('fecha_entrega');
		
		try{
			if(empty($id))
				throw new PreconditionFailedHttpException("Identificador obligatorio", null, 1017);
			if(empty($fecha_entrega))
				throw new PreconditionFailedHttpException("Fecha de entrega obligatoria", null, 1006);
			if(!$this->validar_fecha_espanol($fecha_entrega))
				throw new PreconditionFailedHttpException("Fecha de entrega inválida (yyyy-mm-dd)", null, 1007);
			
			$fecha= date("Y-m-d",strtotime($fecha_entrega));
			$pedidos = $this->getDoctrine()->getRepository(Pedidos::class)->findAllPedidosByDriver($id, $fecha);			
		
			return $pedidos;
		}
		 catch( PreconditionFailedHttpException $e ){
			$response = [];
			$response["code"] = $e->getCode();
			$response["message"] = $e->getMessage();
			return $response;
		 }		
	 }
	 
	 private function validar_fecha_espanol($fecha){
		$valores = explode('-', $fecha);
		$hoy = strtotime(date("Y-m-d",time()));
		if(count($valores) == 3 && checkdate($valores[1], $valores[2], $valores[0])){			
			return true;			
		}
		return false;
	 }
	 
	 private function validar_hora($hora){		 
		$regexHora ="/^([0-1][0-9]|2[0-3])(:)([0-5][0-9])$/";
		if (preg_match($regexHora, $hora)) {
			return true;
		}
		return false;
	 } 
	 
	 private function validar_franja_horaria($hora1, $hora2){		 
		$valores_hora1 = explode(':', $hora1);
		$valores_hora2 = explode(':', $hora2);
		if( $valores_hora2[0] < ($valores_hora1[0] + 1) || $valores_hora2[0] > ($valores_hora1[0] + 8))
				return false;
		return true;	
		
	 } 
	
}
