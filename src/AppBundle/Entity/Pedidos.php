<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pedidos
 *
 * @ORM\Table(name="pedidos")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PedidosRepository")
 */
class Pedidos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * 
	 * @ORM\ManyToOne(targetEntity="Clientes", inversedBy="pedidos")
	 * @ORM\JoinColumn(name="cliente", referencedColumnName="id")
     */
    private $cliente;
	
	/**
     * @var string
     *
     * @ORM\Column(name="nombre_facturacion", type="string", length=150)
     */
    private $nombreFacturacion;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos_facturacion", type="string", length=255)
     */
    private $apellidosFacturacion;
	
    /**
	 *
	 * @ORM\OneToOne(targetEntity="Direcciones")
	 * @ORM\JoinColumn(name="direccion", referencedColumnName="id")
	 */
    private $direccion;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=50)
     */
    private $telefono;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_entrega", type="date")
     */
    private $fechaEntrega;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hora_inicio", type="time")
     */
    private $horaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hora_fin", type="time")
     */
    private $horaFin;

    
    /**
     * @ORM\ManyToOne(targetEntity="Drivers", inversedBy="pedidos")
	 * @ORM\JoinColumn(name="driver", referencedColumnName="id")
     */
    private $driver;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cliente
     *
     * @param integer $cliente
     *
     * @return Pedidos
     */
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return int
     */
    public function getCliente()
    {
        return $this->cliente;
    }
	
	/**
     * Set nombreFacturacion
     *
     * @param string $nombreFacturacion
     *
     * @return Pedidos
     */
    public function setNombreFacturacion($nombreFacturacion)
    {
        $this->nombreFacturacion = $nombreFacturacion;

        return $this;
    }
	
	
    /**
     * Get nombreFacturacion
     *
     * @return string
     */
    public function getNombreFacturacion()
    {
        return $this->nombreFacturacion;
    }

    /**
     * Set apellidosFacturacion
     *
     * @param string $apellidosFacturacion
     *
     * @return Pedidos
     */
    public function setApellidosFacturacion($apellidosFacturacion)
    {
        $this->apellidosFacturacion = $apellidosFacturacion;

        return $this;
    }

    /**
     * Get apellidosFacturacion
     *
     * @return string
     */
    public function getApellidosFacturacion()
    {
        return $this->apellidosFacturacion;
    }
	
    /**
     * Set direccion
     *
     * @param integer $direccion
     *
     * @return Pedidos
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return int
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

	 /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Pedidos
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }
	
    /**
     * Set fechaEntrega
     *
     * @param \DateTime $fechaEntrega
     *
     * @return Pedidos
     */
    public function setFechaEntrega($fechaEntrega)
    {
        $this->fechaEntrega = $fechaEntrega;

        return $this;
    }

    /**
     * Get fechaEntrega
     *
     * @return \DateTime
     */
    public function getFechaEntrega()
    {
        return $this->fechaEntrega->format('Y-d-m');
    }

    /**
     * Set horaInicio
     *
     * @param \DateTime $horaInicio
     *
     * @return Pedidos
     */
    public function setHoraInicio($horaInicio)
    {
        $this->horaInicio = $horaInicio;

        return $this;
    }

    /**
     * Get horaInicio
     *
     * @return \DateTime
     */
    public function getHoraInicio()
    {
        return $this->horaInicio;
    }

    /**
     * Set horaFin
     *
     * @param \DateTime $horaFin
     *
     * @return Pedidos
     */
    public function setHoraFin($horaFin)
    {
        $this->horaFin = $horaFin;

        return $this;
    }

    /**
     * Get horaFin
     *
     * @return \DateTime
     */
    public function getHoraFin()
    {
        return $this->horaFin;
    }

    /**
     * Set driver
     *
     * @param integer $driver
     *
     * @return Pedidos
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return int
     */
    public function getDriver()
    {
        return $this->driver;
    }
}

